# poker hands kata

Some starter code is provided. It is the beginnings of an implementation of a
poker hands comparison tool. In its current state it simply judges hands based
on 'high card' only. There are some basic tests provided.

Your task is to extend the code (and the tests) in order to improve the
implementation to cover more hand types. The highest ranked hand type is
*Royal flush* and the lowest is *high card*.

Refer to https://www.cardplayer.com/rules-of-poker/hand-rankings for rank
descriptions

## Oliver Smart's fork

This is a fork of https://github.com/bennuttall/poker-kata working through the exercise
given in **Cambridge Python User Group (CamPUG)** *Let's Do Some Coding* 09 Jan 2018
https://www.meetup.com/CamPUG/events/242172336/

Follow https://www.pagat.com/poker/rules/ranking.html because this gives a 
comprehensive description of scoring - for instance Aces low in straights.
