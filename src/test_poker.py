#!/usr/bin/env python3

"""unit tests for poker.py"""

import collections
import unittest
from poker import Card, PokerHand, create_deck, deal_hand

# some useful card suit/value constants
T = 'T'
J = 'J'
Q = 'Q'
K = 'K'
A = 'A'
H = 'H'
D = 'D'
S = 'S'
C = 'C'
# you can use integers for the others (but not 10 - use T)


def pokerhand_from_cards(*card_strs):
    """ create a PokerHand object from 5 cards specified as suit/value tuples """
    if len(card_strs) != 5:
        raise RuntimeError('pokerhand_from_cards must be called with 5 card strings')
    cards = []
    for card_str in card_strs:
        if len(card_str) != 2:
            raise RuntimeError('pokerhand_from_cards must be called with card string tuples suit/value')
        card = Card(card_str[0], card_str[1])
        cards.append(card)
    hand = PokerHand(cards)
    return hand


class TestPokerSingleCard(unittest.TestCase):
    """Test poker: Card class"""

    def test_single_card_comparision_ace_spades_equals_ace_diamonds(self):
        self.assertEqual(Card(A, S), Card(A, H))

    def test_single_card_comparision_ace_spades_not_equals_heart_diamonds(self):
        self.assertNotEqual(Card(A, S), Card(K, H))

    def test_single_card_comparision_ace_spades_greater_than_heart_diamonds(self):
        self.assertGreater(Card(A, S), Card(K, H))

    def test_single_card_comparison_two_spades_less_than_ace_spades(self):
        self.assertLess(Card(2, S), Card(A, S))


class TestPokerPokerHand(unittest.TestCase):
    """Test poker: PokerHand class particularly its scoring"""
    def test_multiple_hand_beat_or_ties_with_other_hand(self):
        HandBeatsOther = collections.namedtuple('HandBeatsOther', 'description tie hand_win hand_lose')
        lt = []
        lt.append(('High card ace beats high card queen', False,
                  ((A, S), (2, D), (4, C), (7,  H), (K, S)), 
                  ((Q, S), (3, D), (5, C), (8,  H), (T, S))))
        lt.append(('High card both ace goes to 2nd card and K>T', False,
                  ((A, S), (2, D), (4, C), (7,  H), (K, S)), 
                  ((A, H), (3, D), (5, C), (8,  H), (T, S)))),
        lt.append(('High card same card except lowest and 3 > 2', False,
                  ((A, S), (3, D), (4, C), (7,  H), (K, S)),
                  ((A, D), (2, C), (4, H), (7,  D), (K, H))))
        lt.append(('High card all same cards produces tie', True,
                  ((A, S), (2, D), (4, C), (7,  H), (K, S)),
                  ((A, D), (2, C), (4, H), (7,  D), (K, H))))
        lt.append(('low straight beats any high card', False,
                  ((6, S), (5, D), (4, C), (3,  H), (2, S)),
                  ((A, D), (2, C), (4, H), (7,  D), (K, H))))
        lt.append(('5-4-3-2-A is a straight so beats any high card', False,
                  ((5, S), (4, D), (3, C), (2,  H), (A, S)),
                  ((A, D), (2, C), (4, H), (7,  D), (K, H))))
        lt.append(('5-4-3-2-A straight flush beats a flush', False,
                  ((5, S), (4, S), (3, S), (2,  S), (A, S)),
                  ((A, C), (2, C), (4, C), (7,  C), (K, C))))
        lt.append(('flush beats straight', False,
                  ((A, D), (2, D), (4, D), (7,  D), (K, D)), 
                  ((6, S), (5, D), (4, C), (3,  H), (2, S))))
        lt.append(('4 of a kind beats flush', False,
                  ((2, D), (2, C), (2, H), (2,  S), (3, S)), 
                  ((A, C), (T, C), (4, C), (7,  C), (K, C))))
        lt.append(('4 of a kind beats straight', False,
                  ((2, D), (2, C), (2, H), (2,  S), (3, S)), 
                  ((A, C), (K, H), (Q, D), (J,  C), (T, C))))
        lt.append(('full house beats straight', False,
                  ((2, D), (2, C), (2, H), (3,  D), (3, S)), 
                  ((A, C), (K, H), (Q, D), (J,  C), (T, C))))
        lt.append(('full house 99922 beats 777AA', False,
                  ((9, D), (9, C), (9, H), (2,  D), (2, S)), 
                  ((7, C), (7, H), (7, D), (A,  C), (A, S))))
        lt.append(('three of a kind 22234 beats high card AQT98', False,
                  ((2, D), (2, C), (2, H), (3,  D), (4, S)), 
                  ((A, C), (Q, H), (T, D), (9,  C), (8, S))))
        lt.append(('three of a kind 22235 beats high card 22234', False,
                  ((2, D), (2, C), (2, H), (3,  D), (5, S)), 
                  ((2, C), (2, H), (2, D), (3,  C), (4, S))))
        lt.append(('pair 22346 beats high card AQT98', False,
                  ((2, D), (2, C), (6, H), (3,  D), (4, S)), 
                  ((A, C), (Q, H), (T, D), (9,  C), (8, S))))
 
        test_hands = []
        for l in lt:
            nt = HandBeatsOther(description=l[0], tie=l[1], 
                                hand_win=l[2], hand_lose=l[3])
            test_hands.append(nt)
        for test_hand in test_hands:
            with self.subTest(test_hand=test_hand):
                hand_win = pokerhand_from_cards(*test_hand.hand_win)
                hand_lose = pokerhand_from_cards(*test_hand.hand_lose)
                if test_hand.tie:
                    self.assertEqual(hand_win, hand_lose)
                    self.assertEqual(hand_lose, hand_win)
                    self.assertFalse(hand_win > hand_lose)
                    self.assertFalse(hand_win < hand_lose)
                else:
                    self.assertGreater(hand_win, hand_lose)
                    self.assertLess(hand_lose, hand_win)
                    self.assertNotEqual(hand_win, hand_lose)

    def test_straight(self):
        self.assertEqual(pokerhand_from_cards((6, S), (5, D), (4, C), (3,  H), (2, S)).straight, '6')
        self.assertIsNone(pokerhand_from_cards((6, S), (6, D), (4, C), (3,  H), (2, S)).straight)
        self.assertEqual(pokerhand_from_cards((5, D), (4, C), (3,  H), (2, S), (A, D)).straight, '5')
        self.assertEqual(pokerhand_from_cards((A, D), (K, C), (Q,  H), (J, S), (T, D)).straight, 'A')

    def test_classify(self):
        hc = collections.OrderedDict()
        hc[((J, C), (T, C), (9, C), (8,  C), (7, C))] = ('Straight Flush', J)
        hc[((5, C), (5, H), (5, D), (5,  S), (J, C))] = ('Four of a Kind', '5', J)
        hc[((2, C), (2, H), (2, D), (T,  S), (T, C))] = ('Full House', '2', T)
        hc[((2, C), (T, C), (3, C), (8,  C), (7, C))] = ('Flush', T, '8', '7', '3', '2')
        hc[((6, S), (5, D), (4, C), (3,  H), (2, S))] = ('Straight', '6')
        hc[((5, C), (5, H), (5, D), (7,  S), (J, C))] = ('Three of a Kind', '5', J, '7')
        hc[((5, C), (5, H), (5, D), (J,  S), (7, C))] = ('Three of a Kind', '5', J, '7')
        hc[((5, C), (K, H), (5, D), (K,  S), (A, C))] = ('Two Pairs', K, '5', A)
        hc[((5, C), (Q, H), (5, D), (K,  S), (A, C))] = ('Pair', '5', A, K, Q)
        hc[((A, D), (2, C), (4, H), (7,  D), (K, H))] = ('High Card', 'A', 'K', '7', '4', '2')
        for k, v in hc.items():
            with self.subTest(hand=k, classification=v):
                self.assertEqual(pokerhand_from_cards(*k).classify, v)


class TestPokerCreateDeckAndHand(unittest.TestCase):
    """Test poker: create_deck and deal_hand"""
 
    def test_create_a_deck(self):
        deck = create_deck()
        self.assertEqual(len(deck), 52)
        self.assertTrue(all(isinstance(card, Card) for card in deck))

    def test_creating_a_hand_from_the_deck(self):
        deck = create_deck()
        cards = deal_hand(5, deck)
        self.assertTrue(all(isinstance(card, Card) for card in cards))
        hand = PokerHand(cards)
        self.assertTrue(isinstance(hand, PokerHand))


if __name__ == '__main__':
    unittest.main()
