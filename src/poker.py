from collections import OrderedDict
from itertools import product
from random import shuffle

VALUES = ('2', '3', '4', '5', '6', '7', '8', '9', 'T', 'J', 'Q', 'K', 'A')
SUITS = ('H', 'D', 'S', 'C')

HAND_TYPES = (
    'High Card',
    'Pair',
    'Two Pairs',
    'Three of a Kind',
    'Straight',
    'Flush',
    'Full House',
    'Four of a Kind',
    'Straight Flush'
)


class Card:
    def __init__(self, value, suit):
        value = str(value)
        suit = str(suit)
        if value not in VALUES:
            raise ValueError
        self.value = value
        if suit not in SUITS:
            raise ValueError
        self.suit = suit

    def __repr__(self):
        return '<Card object {}{}>'.format(self.value, self.suit)

    def __eq__(self, other):
        return self.value == other.value

    def __lt__(self, other):
        return self.value_index < other.value_index

    def __gt__(self, other):
        return self.value_index > other.value_index

    @property
    def value_index(self):
        return VALUES.index(self.value)


class PokerHand:
    def __init__(self, cards):
        if any(not isinstance(card, Card) for card in cards):
            raise ValueError
        if len(cards) != 5:
            raise ValueError
        card_set = {repr(card) for card in cards}
        duplicate_cards = len(card_set) < len(cards)
        if duplicate_cards:
            raise ValueError
        self.cards = sorted(cards)

    def __repr__(self):
        return '<PokerHand object {}>'.format(', '.join(str(card) for card in self))

    def __iter__(self):
        return iter(self.cards)

    def __gt__(self, other):
        return self.score > other.score

    def __lt__(self, other):
        return self.score < other.score

    def __eq__(self, other):
        return self.score == other.score

    @property
    def straight(self):
        """ tests whether the hand is a straight and the highest card value
            or None if the hand is not a straight.

            Note aces can count high or low, so A-K-Q-J-T is a straight with high card A and
            5-4-3-2-A is a straight with high card 5
        """
        if self.cards[0].value == '2' and self.cards[1].value == '3' and \
           self.cards[2].value == '4' and self.cards[3].value == '5' and \
           self.cards[4].value == 'A':
            return '5'
        previous_card = None
        for card in self.cards:
            if previous_card is not None:
                if card.value_index != (previous_card.value_index + 1):
                    return None
            previous_card = card
        return previous_card.value

    @property
    def flush(self):
        """tests for flush - all cards same suit

           returns the cards values large to small if there is a flush or
           None otherwise"""
        first_suit = self.cards[0].suit
        for card in self.cards[1:]:
            if card.suit != first_suit:
                return None
        card_values = [card.value for card in reversed(self.cards)]
        return tuple(card_values)

    @property
    def classify_multiple(self):
        """ detects four of a kind, full house, three of a kind ec. 
  
             returns a tuple - 1st item is the hand type from HANDTYPES,
             next items are the card types for that hand."""
        count_card_values = OrderedDict()
        for card in reversed(self.cards):
            count_card_values[card.value] = 1 + count_card_values.get(card.value, 0)
        # by using OrderDict most_cards is ordered by maximum number but with
        # secondary order of card value_index
        most_cards = sorted(count_card_values, key=count_card_values.get, reverse=True)
        if count_card_values[most_cards[0]] == 4:
            return 'Four of a Kind', most_cards[0], most_cards[1]
        elif count_card_values[most_cards[0]] == 3 and count_card_values[most_cards[1]] == 2:
            return 'Full House', most_cards[0], most_cards[1]
        elif count_card_values[most_cards[0]] == 3 and count_card_values[most_cards[1]] == 1:
            return tuple(['Three of a Kind', ] + most_cards)
        elif count_card_values[most_cards[0]] == 2 and count_card_values[most_cards[1]] == 2:
            return tuple(['Two Pairs', ] + most_cards)
        elif count_card_values[most_cards[0]] == 2:
            return tuple(['Pair', ] + most_cards)
        else:
            return None

    @property
    def classify(self):
        """ classifies the hand 

            returns a tuple - 1st item is the hand type from HANDTYPES,
             next items are the card types for that hand.

            So for a hand with four Jacks and 3 would return 
            ('Four of a Kind', 'J', '3')

            For a hand with three 2s and a pair of queens would return
            ('Full House', '2', 'Q')
        """
        straight = self.straight
        flush = self.flush
        classify_multiple = self.classify_multiple
        if straight is not None and flush is not None:
            return 'Straight Flush', straight
        elif classify_multiple is not None:
            return classify_multiple
        elif flush is not None:
            return ('Flush', *flush)
        elif straight is not None:
            return 'Straight', straight
        else:
            return ('High Card', *(card.value for card in reversed(self.cards)))

    @property
    def score(self):
        """ score a hand: returns an integer score that is higher the better the score. """
        classify = self.classify
        multiplier = 100 * 100 * 100 * 100 * 100 * 100 * 100
        my_score = multiplier * HAND_TYPES.index(classify[0])
        for card in self.classify[1:]:
            multiplier = multiplier / 100
            my_score += multiplier * VALUES.index(card)
        return my_score


def create_deck():
    cards = [Card(value, suit) for value, suit in product(VALUES, SUITS)]
    shuffle(cards)
    return cards


def deal_hand(n, deck):
    return [deck.pop() for _ in range(n)]
